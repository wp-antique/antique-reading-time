<?php
/**
 * Plugin Name: Antique Reading Time
 * Plugin URI: https://gitlab.com/wp-antique/plugins/antique-reading-time
 * Description: Display the reading time of a page.
 * Version: 1.0.0
 * Author: Simon Garbin
 * Author URI: simongarbin.com
 * License: GPLv3
 * License URI: https://www.gnu.org/licenses/gpl-3.0.html.en
 * Text Domain: antique-reading-time
 * Domain Path: /languages
 */
/*
  Copyright 2023 Simon Garbin
  Antique Reading Time is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  any later version.

  Antique Reading Time is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Antique Reading Time. If not, see
  https://www.gnu.org/licenses/gpl-3.0.html.en.
 */
/*
  Comments:
  - Every function is prefixed by the plugin name in order to prevent
  name collisions.
  - In almost all functions named arguments are used in order to facilitate
  the understanding of the code.
  - Single quotation marks are used for php/js code, double quotation marks
  for HTML.
 */

include plugin_dir_path(__FILE__) . '/shared/menu.php';
include plugin_dir_path(__FILE__) . '/shared/register.php';
include plugin_dir_path(__FILE__) . '/settings.php';

defined('ABSPATH') || exit;

/*
  ----------------------------------------
  Translation
  ----------------------------------------
 */

add_action(
        hook_name: 'init',
        callback: 'antique_reading_time_load_textdomain'
);

function antique_reading_time_load_textdomain() {
    load_plugin_textdomain(
            domain: 'antique-reading-time',
            deprecated: false,
            plugin_rel_path: dirname(plugin_basename(__FILE__)) . '/languages'
    );
}

function antique_reading_time_translation_dummy() {
    $plugin_description = __('Display the reading time of a page.',
            'antique-section-search');
}

/*
  ----------------------------------------
  Admin Settings
  ----------------------------------------
 */

add_filter(
        hook_name: 'plugin_action_links_' . plugin_basename(__FILE__),
        callback: 'antique_reading_time_settings_link'
);

function antique_reading_time_settings_link(array $links) {

    $url = admin_url(path: 'admin.php?page=antique_reading_time');
    $link = '<a href="' . esc_html($url) . '">'
            . __('Settings', 'antique-reading-time')
            . '</a>';
    $links[] = $link;

    return $links;
}

/*
  ----------------------------------------
  Styles and Scripts
  ----------------------------------------
 */

add_action(
        hook_name: 'wp_enqueue_scripts',
        callback: 'antique_reading_time_style'
);

function antique_reading_time_style() {

    $handle = 'antique-reading-time-style';

    wp_register_style(
            handle: $handle,
            src: plugins_url('/assets/css/style.css', __FILE__),
            deps: array(),
            ver: false,
            media: 'all'
    );

    wp_enqueue_style(handle: $handle);
}

add_action(
        hook_name: 'wp_head',
        callback: 'antique_reading_time_custom_style'
);

add_action(
        hook_name: 'enqueue_block_editor_assets',
        callback: 'antique_reading_time_custom_style'
);

function antique_reading_time_custom_style() {

    $options = get_antique_reading_time_options();

    $heading_has_border = $options['heading_border_bool'] == 'on';
    $content_has_border = $options['content_border_bool'] == 'on';
    $is_antique = $options['antique_style'] == 'on' && wp_get_theme() == 'Antique';

    $heading_bg_color = $is_antique ? 'var(--antique-blocks--heading--bg-color)' :
            $options['heading_bg_color'];
    $heading_font_color = $is_antique ? 'var(--antique-blocks--heading--font-color)' :
            $options['heading_font_color'];
    $heading_border_color = $is_antique ? 'var(--antique-blocks--heading--border-color)' :
            $options['heading_border_color'];
    $heading_border = $heading_has_border ? sprintf(
                    '%spx %s %s',
                    $options['heading_border_width'],
                    $options['heading_border_style'],
                    $heading_border_color
            ) : 'none';

    $content_bg_color = $is_antique ? 'var(--antique-blocks--content--bg-color)' :
            $options['content_bg_color'];
    $content_font_color = $is_antique ? 'var(--antique-blocks--content--font-color)' :
            $options['content_font_color'];
    $content_border_color = $is_antique ? 'var(--antique-blocks--content--border-color)' :
            $options['content_border_color'];
    $content_border = $content_has_border ? sprintf(
                    '%spx %s %s',
                    $options['content_border_width'],
                    $options['content_border_style'],
                    $content_border_color
            ) : 'none';

    $style = sprintf(
            '<style id="antique-reading-time-custom-css">'
            . ':root {'
            . '--antique-reading-time--heading--bg-color: %s;'
            . '--antique-reading-time--heading--font-color: %s;'
            . '--antique-reading-time--heading--border: %s;'
            . '--antique-reading-time--content--bg-color: %s;'
            . '--antique-reading-time--content--font-color: %s;'
            . '--antique-reading-time--content--border: %s;'
            . '}'
            . '</style>',
            $heading_bg_color,
            $heading_font_color,
            $heading_border,
            $content_bg_color,
            $content_font_color,
            $content_border
    );

    echo $style;
}

add_action(
        hook_name: 'wp_enqueue_scripts',
        callback: 'antique_reading_time_pass_options_to_js'
);

function antique_reading_time_pass_options_to_js() {

    if (!is_singular()) {
        return;
    }

    $options = get_antique_reading_time_options();
    $post_meta = antique_reading_time_get_post_meta(
            id: get_the_ID()
    );

    $display = $post_meta['display'];
    $add_subpages = $post_meta['add_subpages'];

    if ($display != 'on') {
        return;
    }

    $reading_speed_db = $options['reading_speed'];
    $reading_speed = is_numeric($reading_speed_db) ? (int) $reading_speed_db : 250;

    $reading_time = __('Reading time', 'antique-reading-time') . ': '
            . __('approx.', 'antique-reading-time') . ' '
            . get_the_antique_reading_time(
                    id: get_the_ID(),
                    as_str: true,
                    reading_speed: $reading_speed,
                    add_subpages: $add_subpages
    );

    $pos_rel = $options['position_rel'];
    $pos_num_as_str = $options['position_num'];
    $pos_num = is_numeric($pos_num_as_str) ? (int) $pos_num_as_str - 1 : 0;
    $pos_tag = $options['position_tag'];

    $pos_antique = $options['position_antique'];
    $theme = wp_get_theme();

    $options_for_js = array(
        'reading_time' => $reading_time,
    );

    if ($pos_antique == 'on' && $theme == 'Antique') {
        $options_for_js['position'] = $pos_antique;
    } else {
        $options_for_js['position'] = array($pos_rel, $pos_num, $pos_tag);
    }

    $handle = 'antique-reading-time-insert';

    wp_register_script(
            handle: $handle,
            src: plugins_url('/assets/js/insert.js', __FILE__),
            deps: array(),
            ver: false,
            in_footer: true
    );

    wp_localize_script(
            handle: $handle,
            object_name: 'antique_reading_time_options',
            l10n: $options_for_js
    );

    wp_enqueue_script(handle: $handle);
}

/*
  ----------------------------------------
  Meta Box
  ----------------------------------------
 */

function antique_reading_time_get_post_meta($id) {

    $meta_key = antique_reading_time_get_meta_strings()['key'];
    $saved_meta = get_post_meta(
            post_id: $id,
            key: $meta_key,
            single: false
    );

    $full_meta = antique_plugins_parse_meta_args(
            args: isset($saved_meta[0]) ? $saved_meta[0] : array(),
            defaults: array(
                'display' => '',
                'add_subpages' => ''
            )
    );

    return $full_meta;
}

function antique_reading_time_get_meta_strings() {

    $meta_strings = array(
        'field' => 'antique_reading_time_meta_field',
        'key' => '_antique_reading_time_meta_key',
        'nonce_name' => 'antique_reading_time_meta_nonce',
        'nonce_action' => 'add_antique_reading_time_meta_nonce',
    );

    return $meta_strings;
}

add_action(
        hook_name: 'add_meta_boxes',
        callback: 'antique_reading_time_meta_box'
);

function antique_reading_time_meta_box() {

    add_meta_box(
            id: 'antique_reading_time',
            title: __('Reading time', 'antique-reading-time'),
            callback: 'antique_reading_time_meta_box_html',
            screen: array(
                'page',
                'post'
            ),
            context: 'side',
            priority: 'high'
    );
}

function antique_reading_time_meta_box_html($post) {

    $meta = antique_reading_time_get_meta_strings();

    $meta_field = $meta['field'];
    $nonce_name = $meta['nonce_name'];
    $nonce_action = $meta['nonce_action'];

    wp_nonce_field(
            action: $nonce_action,
            name: $nonce_name
    );

    $post_meta = antique_reading_time_get_post_meta(
            id: $post->ID
    );

    $display = $post_meta['display'];
    $add_subpages = $post_meta['add_subpages'];

    $is_display_checked = ($display == 'on') ? 'checked' : '';
    $is_add_subpages_checked = ($add_subpages == 'on') ? 'checked' : '';
    ?>

    <div class="components-panel__row">
        <label for="<?php esc_attr_e($meta_field); ?>[display]">
            <?php esc_html_e('Display:', 'antique-reading-time'); ?>
        </label>
        <input
            type="checkbox"
            id="<?php esc_attr_e($meta_field); ?>[display]"
            name="<?php esc_attr_e($meta_field); ?>[display]"
            style="margin: auto 0;"
            <?php echo $is_display_checked; ?>
            >
    </div>

    <div class="components-panel__row">
        <label for="<?php esc_attr_e($meta_field); ?>[add_subpages]">
            <?php esc_html_e('Add subpages:', 'antique-reading-time'); ?>
        </label>
        <input
            type="checkbox"
            id="<?php esc_attr_e($meta_field); ?>[add_subpages]"
            name="<?php esc_attr_e($meta_field); ?>[add_subpages]"
            style="margin: auto 0;"
            <?php echo $is_add_subpages_checked; ?>
            >
    </div>

    <?php
}

add_action(
        hook_name: 'save_post',
        callback: 'antique_reading_time_save_meta'
);

function antique_reading_time_save_meta($post_id) {

    $meta = antique_reading_time_get_meta_strings();

    $meta_field = $meta['field'];
    $meta_key = $meta['key'];

    $nonce_name = $meta['nonce_name'];
    $nonce_action = $meta['nonce_action'];

    if (!isset($_POST[$nonce_name])) {
        return;
    }

    if (!wp_verify_nonce(
                    nonce: $_POST[$nonce_name],
                    action: $nonce_action)
    ) {
        return;
    }

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }

    if (
            isset($_POST['post_type']) && (
            'page' == $_POST['post_type'] || 'post' == $_POST['post_type']
            )
    ) {
        if (!current_user_can('edit_page', $post_id)) {
            return;
        }
    }

    if (!isset($_POST[$meta_field])) {
        return;
    }

    $sanitized = antique_reading_time_sanitize_meta(
            post_meta: $_POST[$meta_field]
    );

    update_post_meta(
            post_id: $post_id,
            meta_key: $meta_key,
            meta_value: $sanitized
    );
}

function antique_reading_time_sanitize_meta($post_meta) {

    $sanitized = array();

    if (isset($post_meta['display'])) {
        $sanitized['display'] = sanitize_key(key: $post_meta['display']);
    } else {
        $sanitized['display'] = '';
    }

    if (isset($post_meta['add_subpages'])) {
        $sanitized['add_subpages'] = sanitize_key(key: $post_meta['add_subpages']);
    } else {
        $sanitized['add_subpages'] = '';
    }

    return $sanitized;
}

/*
  ----------------------------------------
  Shortcode
  ----------------------------------------
 */

add_shortcode(
        tag: 'AntiqueReadingTime',
        callback: 'antique_reading_time_shortcode'
);

function antique_reading_time_shortcode($atts) {

    global $current_user;
    $user_id = ( is_user_logged_in() ? $current_user->ID : NULL );

    if ($user_id == NULL) {
        return;
    }

    $new_atts = antique_reading_time_get_shortcode_atts(atts: $atts);

    $output = '<p class="antique-reading-time">'
            . sprintf(
                    '%s: %s ',
                    __('Reading time', 'antique-reading-time'),
                    __('approx.', 'antique-reading-time')
            )
            . get_the_antique_reading_time(
                    id: get_the_ID(),
                    as_str: true,
                    reading_speed: $new_atts['speed'],
                    add_subpages: $new_atts['subpages']
            )
            . '</p>';

    return $output;
}

function antique_reading_time_get_shortcode_atts($atts) {

    $options = get_antique_reading_time_options();
    $default_reading_speed = is_numeric($options['reading_speed']) ?
            (int) $options['reading_speed'] : 250;

    $new_atts = shortcode_atts(
            pairs: array(
                'subpages' => '',
                'speed' => $default_reading_speed
            ),
            atts: $atts,
            shortcode: 'AntiqueReadingTime'
    );

    if (is_numeric($new_atts['speed'])) {
        if (10 <= $new_atts['speed'] && $new_atts['speed'] <= 1000) {
            $speed = (int) $new_atts['speed'];
        } else {
            $speed = $default_reading_speed;
        }
    } else {
        $speed = $default_reading_speed;
    }

    if ($new_atts['subpages'] == 'on') {
        $subpages = true;
    } else {
        $subpages = false;
    }

    return array(
        'speed' => $speed,
        'subpages' => $subpages,
    );
}

/*
  ----------------------------------------
  Block
  ----------------------------------------
 */

add_action(
        hook_name: 'init',
        callback: 'antique_reading_time_register_block'
);

function antique_reading_time_register_block() {

    $asset_file = include( plugin_dir_path(__FILE__) . '/block/block.asset.php');
    $script_handle = 'antique-reading-time-block-script';

    wp_register_script(
            handle: $script_handle,
            src: plugins_url('block/block.js', __FILE__),
            deps: $asset_file['dependencies'],
            ver: $asset_file['version']
    );

    wp_set_script_translations(
            handle: $script_handle,
            domain: 'antique-reading-time',
            path: plugin_dir_path(__FILE__) . '/languages/'
    );

    $reading_speed = get_option(option: 'antique_reading_time_options')['reading_speed'];
    $reading_times = array(
        'no-subpages' => get_the_antique_reading_time(
                id: isset($_GET['post']) ? $_GET['post'] : get_the_ID(),
                as_str: true,
                reading_speed: $reading_speed,
                add_subpages: ''
        ),
        'with-subpages' => get_the_antique_reading_time(
                id: isset($_GET['post']) ? $_GET['post'] : get_the_ID(),
                as_str: true,
                reading_speed: $reading_speed,
                add_subpages: 'on'
        )
    );

    wp_localize_script(
            handle: $script_handle,
            object_name: 'reading_times',
            l10n: $reading_times
    );

    register_block_type(
            block_type: plugin_dir_path(__FILE__) . '/block/',
            args: array(
                'render_callback' => 'antique_reading_time_render_callback'
            )
    );
}

function antique_reading_time_render_callback($block_attributes) {

    $add_subpages = $block_attributes['addSubpages'];

    return get_the_antique_reading_time_block(add_subpages: $add_subpages);
}

function get_the_antique_reading_time_block($add_subpages = false) {

    $options = get_antique_reading_time_options();
    $reading_speed = $options['reading_speed'];
    $is_closed = $options['on_large_screen'] == 'closed';

    $reading_time = get_the_antique_reading_time(
            id: get_the_ID(),
            as_str: true,
            reading_speed: $reading_speed,
            add_subpages: $add_subpages
    );

    $block = '<div class="antique-reading-time-block antique-block">'
            . '<div class="antique-reading-time-heading-wrap antique-block-heading-wrap">'
            . '<span class="antique-reading-time-heading antique-block-heading">'
            . __('Reading time', 'antique-reading-time')
            . '</span>'
            . '<span class="antique-block-arrow antique-block-show-content'
            . ($is_closed ? ' rotate-arrow' : '')
            . '">'
            . '<i class="fa fa-angle-down"></i>'
            . '</span>'
            . '</div>'
            . '<div class="antique-reading-time-content antique-block-content'
            . ($is_closed ? ' hide-content' : '')
            . '">'
            . '<span>'
            . __('approx.', 'antique-reading-time')
            . ' ' . $reading_time
            . '</span>'
            . '</div>'
            . '</div>';

    return $block;
}

/*
  ----------------------------------------
  Reading Time Calculation
  ----------------------------------------
 */

function get_the_antique_reading_time(
        $id,
        $as_str = true,
        $reading_speed = 250,
        $add_subpages = ''
) {

    /* The reading speed indicates the reading
     * velocity in words per minute.
     */
    /* Check if the author enabled the option that the reading time
     * should also include the reading time of all direct subpages.
     */

    if ($id == '') {
        return '...';
    }

    /* Calculate the reading time of the current page. */
    $reading_time = antique_reading_time_single_post(
            id: $id, reading_speed: $reading_speed
    );

    if ($add_subpages) {
        /* If enabled, iteratively calculate the reading time of each
         * subpage and add it.
         */
        $args = array(
            'post_parent' => $id,
            'post_type' => array('page', 'post'),
        );
        $subpages = get_children($args);

        foreach ($subpages as $subpage) {
            $reading_time += antique_reading_time_single_post(
                    id: $subpage->ID, reading_speed: $reading_speed
            );
        }
    }

    if ($as_str) {
        return antique_reading_time_as_string(reading_time: $reading_time);
    } else {
        return $reading_time;
    }
}

function antique_reading_time_single_post($id, $reading_speed) {

    /* Calculate the reading time of a page by
     * (1) retrieving its content and removing all HTML tags,
     * (2) then count the number of strings separated by blanks
     * (3) and finally rounding the number of words divided
     *     by the reading speed.
     * The result is equal to the number of minutes required
     * to read the page content.
     */
    $content = get_the_content(post: $id);
    $content_no_tags = wp_strip_all_tags($content);

    $num_words = sizeof(explode(' ', $content_no_tags));
    $minutes = round($num_words / $reading_speed);

    return $minutes;
}

function antique_reading_time_as_string($reading_time) {

    if ($reading_time / 60 >= 1) {

        /* If the reading time is longer than 60min (1h), display the
         * reading time as:
         * (number of full hours)h (number of minutes that are left)min
         */
        $hours = strval(floor($reading_time / 60)) . 'h ';
        $minutes = strval(
                        round(($reading_time % 60) / 10) * 10
                ) . 'min';

        return $hours . $minutes;
    } else {

        /* If the reading time is shorter than 60min (1h), display the
         * reading time simply as:
         * (number of minutes)min
         */
        $minutes = strval($reading_time) . 'min';

        return $minutes;
    }
}