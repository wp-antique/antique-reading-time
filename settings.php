<?php

function get_antique_reading_time_default_options() {

    $defaults = array(
        'reading_speed' => '250',
        'position_rel' => 'before',
        'position_num' => '1',
        'position_tag' => 'p',
        'position_antique' => '',
        'heading_font_color' => '#ffffff',
        'heading_bg_color' => '#8e8e8e',
        'heading_border_bool' => '',
        'heading_border_width' => '1',
        'heading_border_style' => 'solid',
        'heading_border_color' => '#000000',
        'content_font_color' => '#000000',
        'content_bg_color' => '#f2f2f2',
        'content_border_bool' => '',
        'content_border_width' => '1',
        'content_border_style' => 'solid',
        'content_border_color' => '#000000',
        'on_large_screen' => 'open',
        'on_small_screen' => 'closed',
        'antique_style' => '',
    );

    return $defaults;
}

function get_antique_reading_time_options() {

    $defaults = get_antique_reading_time_default_options();

    $options = wp_parse_args(
            args: get_option('antique_reading_time_options'),
            defaults: $defaults
    );

    return $options;
}

add_action(
        hook_name: 'admin_menu',
        callback: 'antique_reading_time_options_page'
);

function antique_reading_time_options_page() {

    if (!function_exists('antique_plugins_admin_menu')) {
        add_menu_page(
                page_title: __('Antique Reading Time', 'antique-reading-time'),
                menu_title: __('Antique Reading Time', 'antique-reading-time'),
                capability: 'manage_options',
                menu_slug: 'antique_reading_time',
                callback: 'antique_reading_time_options_page_html'
        );
    } else {
        add_submenu_page(
                parent_slug: 'antique_plugins',
                page_title: __('Antique Plugins', 'antique-reading-time')
                . ' &rsaquo; ' . __('Reading Time', 'antique-reading-time'),
                menu_title: __('Reading Time', 'antique-reading-time'),
                capability: 'manage_options',
                menu_slug: 'antique_reading_time',
                callback: 'antique_reading_time_options_page_html'
        );
    }
}

add_action(
        hook_name: 'admin_init',
        callback: 'antique_reading_time_remove_menu_page'
);

function antique_reading_time_remove_menu_page() {
    if (!function_exists('antique_plugins_admin_menu')) {
        remove_menu_page(menu_slug: 'antique_reading_time');
    }
}

function antique_reading_time_options_page_html() {

    if (!current_user_can(capability: 'manage_options')) {
        return;
    }
    ?>

    <div class="wrap antique-plugin-settings-wrap">

        <h1><?php echo esc_html(get_admin_page_title()); ?></h1>

        <?php settings_errors(setting: 'antique_reading_time_updated_message'); ?>
        <?php settings_errors(setting: 'antique_reading_time_reset_message'); ?>

        <p class="antique-description"><?php
            esc_html_e('This plugin allows you to display the reading time of '
                    . 'a page. When you create or edit pages, there is a '
                    . '"Reading Time" section in the right settings sidebar. '
                    . 'Display the reading time of the page you are editing '
                    . 'by activating "Display". If you also activate "Add '
                    . 'subpages", the reading time is increased by the reading '
                    . 'times of all direct subpages.',
                    'antique-reading-time');
            ?></p>

        <p class="antique-description"><?php
            esc_html_e('If you want to display the reading time at an '
                    . 'arbitrary position within the page content, select the '
                    . '"shortcode" block and insert the shortcode '
                    . '[AntiqueReadingTime]. The reading speed is set with '
                    . 'speed=N, where N stands for an integer between 10 and '
                    . '1000, and by adding subpages="on" the reading time of '
                    . 'the subpages is also taken into account. Example:',
                    'antique-reading-time');
            ?><br>[AntiqueReadingTime speed=240 subpages="on"]</p>

        <form action="options.php" method="post">
            <?php
            settings_fields(option_group: 'antique_reading_time_fields');
            do_settings_sections(page: 'antique_reading_time');
            ?>
            <div class="antique-plugin-submit-buttons-wrap">
                <?php
                submit_button(
                        text: __('Save changes', 'antique-reading-time'),
                        type: 'primary',
                        name: 'updated',
                        wrap: false
                );
                submit_button(
                        text: __('Reset', 'antique-reading-time'),
                        type: 'secondary',
                        name: 'reset',
                        wrap: false
                );
                ?>
            </div>
            <?php antique_reading_time_where_is_used(); ?>
        </form>
    </div>

    <?php
}

function antique_reading_time_settings_sanitize_cb($input) {
    
    if (isset($_POST['updated'])) {

        add_settings_error(
                setting: 'antique_reading_time_updated_message',
                code: 'antique_reading_time_updated_message_code',
                message: __('Your settings have been saved.',
                        'antique-reading-time'),
                type: 'updated'
        );

    } else if (isset($_POST['reset'])) {

        add_settings_error(
                setting: 'antique_reading_time_reset_message',
                code: 'antique_reading_time_reset_message_code',
                message: __('Your settings have been reset.',
                        'antique-reading-time'),
                type: 'updated'
        );

        return array();
    }

    return $input;
}

add_action(
        hook_name: 'admin_init',
        callback: 'antique_reading_time_settings_init'
);

function antique_reading_time_settings_init() {

    $option_name = 'antique_reading_time_options';

    register_setting(
            option_group: 'antique_reading_time_fields',
            option_name: $option_name,
            args: array(
                'sanitize_callback' => 'antique_reading_time_settings_sanitize_cb'
            )
    );

    add_settings_section(
            id: 'antique_reading_time_general_section',
            title: __('General', 'antique-reading-time'),
            callback: 'antique_reading_time_general_section_cb',
            page: 'antique_reading_time'
    );

    add_settings_field(
            id: 'reading_speed',
            title: __('Reading speed:', 'antique-reading-time'),
            callback: 'antique_reading_time_field_reading_speed_cb',
            page: 'antique_reading_time',
            section: 'antique_reading_time_general_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'reading_speed',
            )
    );

    add_settings_section(
            id: 'antique_reading_time_position_section',
            title: __('Position', 'antique-reading-time'),
            callback: 'antique_reading_time_position_section_cb',
            page: 'antique_reading_time'
    );

    add_settings_field(
            id: 'position',
            title: __('Position:', 'antique-reading-time'),
            callback: 'antique_reading_time_field_position_cb',
            page: 'antique_reading_time',
            section: 'antique_reading_time_position_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'position',
            )
    );
    
    add_settings_section(
            id: 'antique_reading_time_settings_title_section',
            title: __('Block Title', 'antique-reading-time'),
            callback: 'antique_reading_time_settings_title_section_cb',
            page: 'antique_reading_time'
    );

    add_settings_field(
            id: 'heading_font_color',
            title: __('Font color:', 'antique-reading-time'),
            callback: 'antique_reading_time_field_heading_font_color_cb',
            page: 'antique_reading_time',
            section: 'antique_reading_time_settings_title_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'heading_font_color',
            )
    );

    add_settings_field(
            id: 'heading_bg_color',
            title: __('Background color:', 'antique-reading-time'),
            callback: 'antique_reading_time_field_heading_bg_color_cb',
            page: 'antique_reading_time',
            section: 'antique_reading_time_settings_title_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'heading_bg_color',
            )
    );

    add_settings_field(
            id: 'heading_border',
            title: __('Border:', 'antique-reading-time'),
            callback: 'antique_reading_time_field_heading_border_cb',
            page: 'antique_reading_time',
            section: 'antique_reading_time_settings_title_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'heading_border',
            )
    );

    add_settings_section(
            id: 'antique_reading_time_settings_content_section',
            title: __('Block Content', 'antique-reading-time'),
            callback: 'antique_reading_time_settings_content_section_cb',
            page: 'antique_reading_time'
    );

    add_settings_field(
            id: 'content_font_color',
            title: __('Font color:', 'antique-reading-time'),
            callback: 'antique_reading_time_field_content_font_color_cb',
            page: 'antique_reading_time',
            section: 'antique_reading_time_settings_content_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'content_font_color',
            )
    );

    add_settings_field(
            id: 'content_bg_color',
            title: __('Background color:', 'antique-reading-time'),
            callback: 'antique_reading_time_field_content_bg_color_cb',
            page: 'antique_reading_time',
            section: 'antique_reading_time_settings_content_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'content_bg_color',
            )
    );

    add_settings_field(
            id: 'content_border',
            title: __('Border:', 'antique-reading-time'),
            callback: 'antique_reading_time_field_content_border_cb',
            page: 'antique_reading_time',
            section: 'antique_reading_time_settings_content_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'content_border',
            )
    );

    add_settings_field(
            id: 'antique_style',
            title: '',
            callback: 'antique_reading_time_field_antique_style_cb',
            page: 'antique_reading_time',
            section: 'antique_reading_time_settings_content_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'antique_style',
            )
    );

    add_settings_section(
            id: 'antique_reading_time_settings_behaviour_section',
            title: __('Behaviour', 'antique-reading-time'),
            callback: 'antique_reading_time_settings_behaviour_section_cb',
            page: 'antique_reading_time'
    );

    add_settings_field(
            id: 'on_large_screen',
            title: __('Large screen:', 'antique-reading-time'),
            callback: 'antique_reading_time_field_on_large_screen_cb',
            page: 'antique_reading_time',
            section: 'antique_reading_time_settings_behaviour_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'on_large_screen',
            )
    );

    add_settings_field(
            id: 'on_small_screen',
            title: __('Small screen:', 'antique-reading-time'),
            callback: 'antique_reading_time_field_on_small_screen_cb',
            page: 'antique_reading_time',
            section: 'antique_reading_time_settings_behaviour_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'on_small_screen',
            )
    );
}

function antique_reading_time_general_section_cb($args) {
    
}

function antique_reading_time_field_reading_speed_cb($args) {

    $field_id = esc_attr($args['label_for']);

    $option_name = esc_attr($args['option_name']);
    $option = get_antique_reading_time_options()[$field_id];
    ?>

    <input type="number"
           id="<?php echo $field_id; ?>"
           name="<?php echo $option_name; ?>[<?php echo $field_id; ?>]"
           value="<?php echo is_numeric($option) ? esc_attr($option) : 250 ?>"
           min="10" max="1000" step="10"
           style="width: 75px"
           autocomplete="off"
           >
    <label for="<?php echo $field_id; ?>">
        <?php esc_html_e('words/minute', 'antique-reading-time'); ?>
    </label>

    <p><?php
        esc_html_e('The reading speed indicates how many words are read on '
                . 'average per minute and is used to calculate the approximate '
                . 'reading time of a page. The value specified above is also '
                . 'used as the default reading speed for the shortcode.',
                'antique-reading-time');
        ?></p>

    <?php
}

function antique_reading_time_position_section_cb($args) {
    ?>

    <p><?php
        esc_html_e('Specify the position where the reading time is '
                . 'displayed. This option does not apply to shortcodes.',
                'antique-reading-time');
        ?></p>

    <?php
}

function antique_reading_time_field_position_cb($args) {

    $field_id = esc_attr($args['label_for']);
    $str_position_rel = esc_attr($field_id . '_rel');
    $str_position_num = esc_attr($field_id . '_num');
    $str_position_tag = esc_attr($field_id . '_tag');
    $str_position_antique = esc_attr($field_id . '_antique');

    $option_name = esc_attr($args['option_name']);
    $options = get_antique_reading_time_options();
    ?>

    <div style="
         display: flex;
         flex-direction: row;
         align-items: center;
         margin-bottom: 15px;
         ">

        <select id="<?php echo $str_position_rel; ?>"
                name="<?php echo $option_name; ?>[<?php echo $str_position_rel; ?>]"
                style="margin-right: 4px;"
                autocomplete="off"
                >
            <option value="before"
            <?php
            echo isset($options[$str_position_rel]) ?
                    ( selected($options[$str_position_rel], 'before', false) ) : ( '' );
            ?>
                    ><?php esc_html_e('before', 'antique-reading-time'); ?></option>
            <option value="after"
            <?php
            echo isset($options[$str_position_rel]) ?
                    ( selected($options[$str_position_rel], 'after', false) ) : ( '' );
            ?>
                    ><?php esc_html_e('after', 'antique-reading-time'); ?></option>
        </select>

        <input type="number"
               id="<?php echo $str_position_num; ?>"
               name="<?php echo $option_name; ?>[<?php echo $str_position_num; ?>]"
               value="<?php echo is_numeric($options[$str_position_num]) ? $options[$str_position_num] : 1; ?>"
               min="1" max="5"
               style="width: 50px"
               autocomplete="off"
               >
        <label for="<?php echo esc_attr($str_position_num); ?>"
               style="margin-right: 4px;"
               >
            .
        </label>

        <select id="<?php echo $str_position_tag; ?>"
                name="<?php echo $option_name; ?>[<?php echo $str_position_tag; ?>]"
                autocomplete="off"
                >
            <option value="p"
            <?php
            echo isset($options[$str_position_tag]) ?
                    ( selected($options[$str_position_tag], 'p', false) ) : ( '' );
            ?>
                    >p</option>
            <option value="h1"
            <?php
            echo isset($options[$str_position_tag]) ?
                    ( selected($options[$str_position_tag], 'h1', false) ) : ( '' );
            ?>
                    >h1</option>
            <option value="h2"
            <?php
            echo isset($options[$str_position_tag]) ?
                    ( selected($options[$str_position_tag], 'h2', false) ) : ( '' );
            ?>
                    >h2</option>
        </select>

    </div>


    <input type="checkbox"
           id="<?php echo $str_position_antique; ?>"
           name="<?php echo esc_attr($option_name); ?>[<?php echo $str_position_antique; ?>]"
           <?php echo $options[$str_position_antique] != '' ? 'checked="checked"' : ''; ?>
           autocomplete="off"
           >
    <label><?php
        esc_html_e('Display the reading time at the top of the page content. '
                . 'This functionality is available only if you are using the '
                . 'Antique theme. Otherwise the reading time is displayed '
                . 'at the position specified above.',
                'antique-reading-time');
        ?></label>

    <?php
}

function antique_reading_time_settings_title_section_cb($args) {
    
}

function antique_reading_time_field_heading_font_color_cb($args) {

    $field_id = esc_attr($args['label_for']);

    $option_name = esc_attr($args['option_name']);
    $options = get_antique_reading_time_options();
    ?>

    <input type="text"
           class="color-field"
           id="<?php echo $field_id; ?>"
           name="<?php echo $option_name; ?>[<?php echo $field_id; ?>]"
           value="<?php echo esc_attr($options[$field_id]); ?>"
           data-default-color="#ffffff"
           autocomplete="off"
           >

    <?php
}

function antique_reading_time_field_heading_bg_color_cb($args) {

    $field_id = esc_attr($args['label_for']);

    $option_name = esc_attr($args['option_name']);
    $options = get_antique_reading_time_options();
    ?>

    <input type="text"
           class="color-field"
           id="<?php echo $field_id; ?>"
           name="<?php echo $option_name; ?>[<?php echo $field_id; ?>]"
           value="<?php echo esc_attr($options[$field_id]); ?>"
           data-default-color="#8e8e8e"
           autocomplete="off"
           >

    <?php
}

function antique_reading_time_field_heading_border_cb($args) {

    $option_name = $args['option_name'];
    $options = get_antique_reading_time_options();

    $field_id = esc_attr($args['label_for']);
    $str_border_bool = esc_attr($field_id . '_bool');
    $str_border_width = esc_attr($field_id . '_width');
    $str_border_style = esc_attr($field_id . '_style');
    $str_border_color = esc_attr($field_id . '_color');
    ?>

    <div class="border-customization">

        <input type="checkbox"
               class="enable-border"
               id="<?php echo $str_border_bool; ?>"
               name="<?php echo esc_attr($option_name); ?>[<?php echo $str_border_bool; ?>]"
               <?php echo $options[$str_border_bool] != '' ? 'checked="checked"' : ''; ?>
               autocomplete="off"
               >

        <div class="border-settings">
            <input type="number"
                   id="<?php echo $str_border_width; ?>"
                   name="<?php echo esc_attr($option_name); ?>[<?php echo $str_border_width; ?>]"
                   value="<?php echo esc_attr($options[$str_border_width]); ?>"
                   min="1" max="10"
                   style="width: 60px"
                   autocomplete="off"
                   >
            <label for="<?php echo $str_border_width; ?>">px</label>

            <select id="<?php echo $str_border_style; ?>"
                    name="<?php echo esc_attr($option_name); ?>[<?php echo $str_border_style; ?>]"
                    autocomplete="off"
                    >

                <option value="solid" <?php
                echo isset($options[$str_border_style]) ?
                        ( selected($options[$str_border_style], 'solid', false) ) :
                        ( '' );
                ?>
                        >solid</option>

                <option value="double" <?php
                echo isset($options[$str_border_style]) ?
                        ( selected($options[$str_border_style], 'double', false) ) :
                        ( '' );
                ?>
                        >double</option>
            </select>

            <input type="text"
                   class="color-field"
                   id="<?php echo $str_border_color; ?>"
                   name="<?php echo esc_attr($option_name); ?>[<?php echo $str_border_color; ?>]"
                   value="<?php echo esc_attr($options[$str_border_color]); ?>"
                   data-default-color="#000000"
                   autocomplete="off"
                   >
        </div>

    </div>

    <?php
}

function antique_reading_time_settings_content_section_cb($args) {
    
}

function antique_reading_time_field_content_font_color_cb($args) {

    $field_id = esc_attr($args['label_for']);

    $option_name = esc_attr($args['option_name']);
    $options = get_antique_reading_time_options();
    ?>

    <input type="text"
           class="color-field"
           id="<?php echo $field_id; ?>"
           name="<?php echo $option_name; ?>[<?php echo $field_id; ?>]"
           value="<?php echo esc_attr($options[$field_id]); ?>"
           data-default-color="#000000"
           autocomplete="off"
           >

    <?php
}

function antique_reading_time_field_content_bg_color_cb($args) {

    $field_id = esc_attr($args['label_for']);

    $option_name = esc_attr($args['option_name']);
    $options = get_antique_reading_time_options();
    ?>

    <input type="text"
           class="color-field"
           id="<?php echo $field_id; ?>"
           name="<?php echo $option_name; ?>[<?php echo $field_id; ?>]"
           value="<?php echo esc_attr($options[$field_id]); ?>"
           data-default-color="#f2f2f2"
           autocomplete="off"
           >

    <?php
}

function antique_reading_time_field_content_border_cb($args) {

    $option_name = $args['option_name'];
    $options = get_antique_reading_time_options();

    $field_id = esc_attr($args['label_for']);
    $str_border_bool = esc_attr($field_id . '_bool');
    $str_border_width = esc_attr($field_id . '_width');
    $str_border_style = esc_attr($field_id . '_style');
    $str_border_color = esc_attr($field_id . '_color');
    ?>

    <div class="border-customization">

        <input type="checkbox"
               class="enable-border"
               id="<?php echo $str_border_bool; ?>"
               name="<?php echo esc_attr($option_name); ?>[<?php echo $str_border_bool; ?>]"
               <?php echo $options[$str_border_bool] != '' ? 'checked="checked"' : ''; ?>
               autocomplete="off"
               >

        <div class="border-settings">
            <input type="number"
                   id="<?php echo $str_border_width; ?>"
                   name="<?php echo esc_attr($option_name); ?>[<?php echo $str_border_width; ?>]"
                   value="<?php echo esc_attr($options[$str_border_width]); ?>"
                   min="1" max="10"
                   style="width: 60px"
                   autocomplete="off"
                   >
            <label for="<?php echo $str_border_width; ?>">px</label>

            <select id="<?php echo $str_border_style; ?>"
                    name="<?php echo esc_attr($option_name); ?>[<?php echo $str_border_style; ?>]"
                    autocomplete="off"
                    >

                <option value="solid" <?php
                echo isset($options[$str_border_style]) ?
                        ( selected($options[$str_border_style], 'solid', false) ) :
                        ( '' );
                ?>
                        >solid</option>

                <option value="double" <?php
                echo isset($options[$str_border_style]) ?
                        ( selected($options[$str_border_style], 'double', false) ) :
                        ( '' );
                ?>
                        >double</option>
            </select>

            <input type="text"
                   class="color-field"
                   id="<?php echo $str_border_color; ?>"
                   name="<?php echo esc_attr($option_name); ?>[<?php echo $str_border_color; ?>]"
                   value="<?php echo esc_attr($options[$str_border_color]); ?>"
                   data-default-color="#000000"
                   autocomplete="off"
                   >
        </div>

    </div>

    <?php
}

function antique_reading_time_field_antique_style_cb($args) {

    $field_id = esc_attr($args['label_for']);

    $option_name = esc_attr($args['option_name']);
    $options = get_antique_reading_time_options();
    ?>

    <input type="checkbox"
           id="<?php echo $field_id; ?>"
           name="<?php echo $option_name; ?>[<?php echo $field_id; ?>]"
           <?php echo $options[$field_id] != '' ? 'checked="checked"' : ''; ?>
           autocomplete="off"
           >
    <label for="<?php echo $field_id; ?>"><?php
        esc_html_e('Apply the colors set within the Antique theme. This '
                . 'option only works if the Antique theme is installed and '
                . 'activated. Upon activation of another theme the colors '
                . 'specified above are used.',
                'antique-reading-time');
        ?></label>

    <?php
}

function antique_reading_time_settings_behaviour_section_cb($args) {
    
}

function antique_reading_time_field_on_large_screen_cb($args) {

    $field_id = esc_attr($args['label_for']);

    $option_name = esc_attr($args['option_name']);
    $options = get_antique_reading_time_options();
    ?>

    <select id="<?php echo $field_id; ?>"
            name="<?php echo esc_attr($option_name); ?>[<?php echo $field_id; ?>]"
            autocomplete="off"
            >

        <option value="open" <?php
        echo isset($options[$field_id]) ?
                ( selected($options[$field_id], 'open', false) ) :
                ( '' );
        ?>
                ><?php esc_html_e('open', 'antique-reading-time'); ?></option>

        <option value="closed" <?php
        echo isset($options[$field_id]) ?
                ( selected($options[$field_id], 'closed', false) ) :
                ( '' );
        ?>
                ><?php esc_html_e('closed', 'antique-reading-time'); ?></option>
    </select>

    <?php
}

function antique_reading_time_field_on_small_screen_cb($args) {

    $field_id = esc_attr($args['label_for']);

    $option_name = esc_attr($args['option_name']);
    $options = get_antique_reading_time_options();
    ?>

    <select id="<?php echo $field_id; ?>"
            name="<?php echo esc_attr($option_name); ?>[<?php echo $field_id; ?>]"
            autocomplete="off"
            >

        <option value="open" <?php
        echo isset($options[$field_id]) ?
                ( selected($options[$field_id], 'open', false) ) :
                ( '' );
        ?>
                ><?php esc_html_e('open', 'antique-reading-time'); ?></option>

        <option value="closed" <?php
        echo isset($options[$field_id]) ?
                ( selected($options[$field_id], 'closed', false) ) :
                ( '' );
        ?>
                ><?php esc_html_e('closed', 'antique-reading-time'); ?></option>
    </select>

    <?php
}

function antique_reading_time_where_is_used() {

    $theme_is_antique = wp_get_theme() == 'Antique';
    
    $search_for_block = "<!-- wp:antique-reading-time/block /-->";
    $search_for_sc = "<!-- wp:shortcode -->\n[AntiqueReadingTime";
    
    $plugin_meta_key = '_antique_reading_time_meta_key_display';
    $theme_meta_key = '_antique_theme_reading_time_meta_key';

    $query = new WP_Query(array(
        'post_type' => array('post', 'page'),
        'posts_per_page' => -1
    ));

    $pages = array();

    if ($query->have_posts()) {
        while ($query->have_posts()) {

            $query->the_post();
            $id = get_the_ID();

            // Check if inserted into content with block
            $with_block = (bool) strpos(
                                haystack: get_the_content($id),
                                needle: $search_for_block
                        );
            // Check if inserted into content with shortcode
            $with_sc = (bool) strpos(
                                haystack: get_the_content($id),
                                needle: $search_for_sc
                        );
            // Check if inserted into content with plugin meta box
            $meta_exists = metadata_exists(
                    meta_type: 'post',
                    object_id: $id,
                    meta_key: $plugin_meta_key
            );

            if ($meta_exists) {
                $meta = get_post_meta(
                        post_id: get_the_ID(),
                        key: $plugin_meta_key,
                        single: true
                );
                $with_plugin_meta = $meta != '';
            } else {
                $with_plugin_meta = false;
            }
            
            $is_in_content = in_array(
                    needle: true,
                    haystack: array(
                        $with_block,
                        $with_sc,
                        $with_plugin_meta,
                    ),
                    strict: true,
            );

            // Check if inserted into sidebar with Antique theme meta box
            if ($theme_is_antique) {

                $meta_exists = metadata_exists(
                        meta_type: 'post',
                        object_id: $id,
                        meta_key: $theme_meta_key
                );

                if ($meta_exists) {
                    $meta = get_post_meta(
                            post_id: get_the_ID(),
                            key: $theme_meta_key,
                            single: true
                    );
                    $meta_is_checked = $meta != '';
                } else {
                    $meta_is_checked = false;
                }
            } else {
                $meta_is_checked = false;
            }

            $post_type_en = get_post_type();
            if ($post_type_en == 'page') {
                $post_type = esc_html__('page', 'antique-reading-time');
            } else if ($post_type_en == 'post') {
                $post_type = esc_html__('post', 'antique-reading-time');
            } else {
                $post_type = $post_type_en;
            }

            if ($is_in_content && $meta_is_checked) {
                $pages[$id] = array(
                    'post_type' => $post_type,
                    'title' => get_the_title(),
                    'loc' => esc_html__('content, sidebar', 'antique-reading-time'),
                    'link' => get_permalink(),
                );
            } else if ($is_in_content && !$meta_is_checked) {
                $pages[$id] = array(
                    'post_type' => $post_type,
                    'title' => get_the_title(),
                    'loc' => esc_html__('content', 'antique-reading-time'),
                    'link' => get_permalink(),
                );                
            } else if (!$is_in_content && $meta_is_checked) {
                $pages[$id] = array(
                    'post_type' => $post_type,
                    'title' => get_the_title(),
                    'loc' => esc_html__('sidebar', 'antique-reading-time'),
                    'link' => get_permalink(),
                );                
            }
        }
    }

    ksort($pages);

    wp_reset_postdata();
    ?>

    <div class="antique-plugin-table-wrap">
        <p><?php
            esc_html_e('Pages and posts where the reading time is displayed:',
                    'antique-reading-time');
            ?></p>
        <div id="antique-plugin-toggle-table">
            <span class="show is-displayed"
                  ><?php esc_html_e('Show', 'antique-reading-time'); ?></span>
            <span class="hide"
                  ><?php esc_html_e('Hide', 'antique-reading-time'); ?></span>
        </div>

        <table class="antique-plugin-pages-table">
            <tr>
                <th><?php esc_html_e('ID', 'antique-reading-time'); ?></th>
                <th><?php esc_html_e('type', 'antique-reading-time'); ?></th>
                <th><?php esc_html_e('title', 'antique-reading-time'); ?></th>
                <th><?php esc_html_e('location', 'antique-reading-time'); ?></th>
                <th><?php esc_html_e('link', 'antique-reading-time'); ?></th>
            </tr>
            <?php
            foreach ($pages as $ID => $page) {
                ?>
                <tr>
                    <td><?php esc_html_e($ID); ?></td>
                    <td><?php echo $page['post_type']; ?></td>
                    <td><?php echo $page['title']; ?></td>
                    <td><?php echo $page['loc']; ?></td>
                    <td><a href="<?php echo $page['link']; ?>"
                           target="_blank"><?php
                               esc_html_e('link', 'antique-reading-time');
                               ?></a></td>
                </tr>
                <?php
            }
            ?>
        </table>
    </div>

    <?php
}
