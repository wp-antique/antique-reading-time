��    2      �      <      <     =     K     [  �   p     -  	   ?     I     W     c  �   k  #   6     Z     c     o     w     |  a       �  4   �     $  	   -     7     D     S     `     f     s     |     �  c   �  �   �  [  �     1
     P
     o
     u
     }
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
  �  �
     x     �     �  �   �     �  	   �     �     �     �  �   �  "   �  	   �     �  	     
          z       �  8   �     �  	   �     �                     ,     B     P     Y  m   m  �   �  d  �  *   ;  '   f     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �   Add subpages: Antique Plugins Antique Reading Time Apply the colors set within the Antique theme. This option only works if the Antique theme is installed and activated. Upon activation of another theme the colors specified above are used. Background color: Behaviour Block Content Block Title Border: Display the reading time at the top of the page content. This functionality is available only if you are using the Antique theme. Otherwise the reading time is displayed at the position specified above. Display the reading time of a page. Display: Font color: General Hide ID If you want to display the reading time at an arbitrary position within the page content, select the "shortcode" block and insert the shortcode [AntiqueReadingTime]. The reading speed is set with speed=N, where N stands for an integer between 10 and 1000, and by adding subpages="on" the reading time of the subpages is also taken into account. Example: Large screen: Pages and posts where the reading time is displayed: Position Position: Reading Time Reading speed: Reading time Reset Save changes Settings Show Small screen: Specify the position where the reading time is displayed. This option does not apply to shortcodes. The reading speed indicates how many words are read on average per minute and is used to calculate the approximate reading time of a page. The value specified above is also used as the default reading speed for the shortcode. This plugin allows you to display the reading time of a page. When you create or edit pages, there is a "Reading Time" section in the right settings sidebar. Display the reading time of the page you are editing by activating "Display". If you also activate "Add subpages", the reading time is increased by the reading times of all direct subpages. Your settings have been reset. Your settings have been saved. after approx. before closed content content, sidebar link location open page post sidebar title type words/minute Project-Id-Version: Antique Reading Time
Report-Msgid-Bugs-To: 
Last-Translator: 
Language-Team: Simon Garbin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
POT-Creation-Date: 2023-06-01 20:12+0200
PO-Revision-Date: 2023-06-01 20:13+0200
Language: de_DE
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0.1
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __;_e;esc_attr__;esc_attr_e;esc_html__;esc_html_e
X-Poedit-SearchPath-0: antique-reading-time.php
X-Poedit-SearchPath-1: settings.php
X-Poedit-SearchPath-2: shared/menu.php
X-Poedit-SearchPath-3: block/block.js
X-Poedit-SearchPath-4: block/block.json
 Unterseiten hinzufügen: Antique Plugins Antique Lesezeit Verwende die Farben, die im Antique-Theme festgelegt wurden. Diese Einstellung kann nur angewandt werden, wenn das Antique-Theme installiert und aktiviert ist. Sobald du ein anderes Theme aktivierst, werden die oben festgelegten Farben verwendet. Hintergrundfarbe: Verhalten Block-Inhalt Block-Titel Rahmen: Zeige die Lesezeit am Anfang des Seiteninhalts an. Diese Einstellung steht dir nur zur Verfügung, wenn das Antique-Theme installiert und aktiviert ist, anderenfalls wird die Lesezeit an der oben festgelegten Stelle angezeigt. Zeige die Lesezeit einer Seite an. Anzeigen: Schriftfarbe: Allgemein Verstecken ID Mit dem Shortcode [AntiqueReadingTime] kannst du die Lesezeit an einer beliebigen Stelle im Seiteninhalt anzeigen. Wähle dafür den "Shortcode"-Block und füge dort den Shortcode ein. Mit speed=N, wobei N für eine natürliche Zahl zwischen 10 und 1000 steht, kannst du die Lesegeschwindigkeit festlegen und mit subpages="on" die Lesezeit der Unterseiten dazurechnen. Beispiel: Großer Bildschirm: Seiten und Posts, auf denen die Lesezeit angezeigt wird: Position Position: Lesezeit Lesegeschwindigkeit: Lesezeit Zurücksetzen Änderungen speichern Einstellungen Anzeigen Kleiner Bildschirm: Lege die Stelle fest, an der die Lesezeit angezeigt wird. Diese Einstellung hat keine Wirkung auf Shortcodes. Die Lesegeschwindigkeit gibt an, wie viele Wörter pro Minute im Durchschnitt gelesen werden, und wird gebraucht, um die Lesezeit einer Seite zu berechnen. Der oben festgelegte Wert wird auch als voreingestellte Lesezeit für den Shortcode verwendet. Mit diesem Plugin kannst du auf deinen Seiten die Lesezeit anzeigen. Wenn du eine Seite erstellst oder bearbeitest, siehst auf der rechten Seitenleiste unter den Einstellungen den Abschnitt "Lesezeit". Zeige die Lesezeit an, indem du "Anzeigen" aktivierst. Willst du die Lesezeit aller direkten Unterseiten dazurechnen, aktiviere "Unterseiten hinzufügen". Deine Einstellungen wurden zurückgesetzt. Deine Einstellungen wurden gespeichert. nach ca. vor zu Inhalt Inhalt, Seitenleiste Link Stelle offen Seite Post Seitenleiste Titel Typ Wörter/Minute 