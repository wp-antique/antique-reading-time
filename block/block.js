(function (blocks, element, components, blockEditor, i18n) {
    var el = element.createElement;
    var registerBlockType = blocks.registerBlockType;
    var useBlockProps = blockEditor.useBlockProps;
    var __ = i18n.__;
    var CheckboxControl = components.CheckboxControl;
    var InspectorControls = blockEditor.InspectorControls;
    var PanelBody = components.PanelBody;

    const cls_block = 'antique-reading-time-block antique-block';
    const cls_heading_wrap = 'antique-reading-time-heading-wrap antique-block-heading-wrap';
    const cls_heading = 'antique-reading-time-heading antique-block-heading';
    const cls_content_wrap = 'antique-reading-time-content antique-block-content';
    const cls_arrow = 'antique-block-arrow antique-block-show-content';
    const cls_arrow_icon = 'fa fa-angle-down';

    registerBlockType('antique-reading-time/block', {
        apiVersion: 2,
        title: __('Antique Reading Time', 'antique-reading-time'),
        icon: 'info-outline',
        category: 'widgets',
        attributes: {
            addSubpages: {
                type: 'boolean',
                default: false
            }
        },

        edit: function (props) {
            var attributes = props.attributes;

            if (attributes.addSubpages === true) {
                var reading_time = reading_times['with-subpages'];
            } else {
                var reading_time = reading_times['no-subpages'];
            }

            function onChangeAddSubpages(bool) {
                props.setAttributes({addSubpages: bool});
            }

            return [
                el('div', useBlockProps(),
                        el('div', {className: cls_block},
                                el('div', {className: cls_heading_wrap},
                                        el('span', {className: cls_heading}, __('Reading time', 'antique-reading-time')),
                                        el('span', {className: cls_arrow},
                                                el('i', {className: cls_arrow_icon})
                                                )
                                        ),
                                el('div', {className: cls_content_wrap},
                                        el('span', {}, reading_time)
                                        )
                                ),
                        el(InspectorControls, {},
                                el(PanelBody, {
                                    title: __('Settings', 'antique-reading-time'),
                                    initialOpen: true
                                },
                                        el(CheckboxControl, {
                                            label: __('Add subpages', 'antique-reading-time'),
                                            type: 'checkbox',
                                            name: 'add-subpages',
                                            checked: attributes.addSubpages,
                                            onChange: onChangeAddSubpages
                                        })
                                        )
                                )
                        )
            ];
        },
        save: function (props) {
            return null;
        }
    });
})(
        window.wp.blocks,
        window.wp.element,
        window.wp.components,
        window.wp.blockEditor,
        window.wp.i18n,
        );

