(function () {

    const options = antique_reading_time_options;
    const reading_time = options['reading_time'];
    const position = options['position'];

    var reading_time_p = document.createElement('p');
    reading_time_p.classList.add('antique-reading-time');
    reading_time_p.innerHTML = reading_time;

    if (!Array.isArray(position)) {
        var content = document.getElementById('page-content');
        if (content === null) {
            return;
        }
        
        content.prepend(reading_time_p);
        return;
    }

    const [pos_rel, pos_num, pos_tag] = [...position];

    var nodes = document.getElementsByTagName(pos_tag);
    if (nodes.length === 0 || pos_num >= nodes.length) {
        return;
    }

    var sibling = nodes[pos_num];
    if (sibling === null) {
        return;
    }

    var parent = sibling.parentNode;
    if (parent === null) {
        return;
    }

    if (pos_rel === 'before') {
        parent.insertBefore(reading_time_p, sibling);
    } else {
        parent.insertBefore(reading_time_p, sibling.nextSibling);
    }

}
)($);